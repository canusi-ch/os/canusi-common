name := "canusi-common"

version := "0.1"

scalaVersion := "2.12.7"

resourceDirectory in Compile := baseDirectory.value / "src"/ "main" / "resources"
resourceDirectory in Test := baseDirectory.value / "src"/ "test" / "resources"

scalaSource in Compile := baseDirectory.value / "src"/ "main" / "scala"
scalaSource in Test := baseDirectory.value / "src"/ "test" / "scala"

libraryDependencies ++= Seq(
    "org.scala-lang" % "scala-compiler" % "2.12.7" // required for mavok dependencies
  , "org.scalatest" %% "scalatest" % "3.0.8" % "test"
  , "org.specs2" %% "specs2-core" % "4.7.1" % Test
  , "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
)


// MiniApps -------------------------- Job
lazy val akkaVersion = "2.5.23"
lazy val scalaTestVersion = "3.0.5"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "org.scalatest" %% "scalatest" % scalaTestVersion,
  "org.scala-graph" %% "graph-core" % "1.13.0" withSources(),
  "com.lightbend.akka" %% "akka-stream-alpakka-slick" % "1.1.2",
  "org.apache.avro"  % "avro" %  "1.9.1" withSources(),
  "com.univocity" % "univocity-parsers" % "2.8.3" withSources(),
  "org.slf4j" % "slf4j-jdk14" % "1.7.30" withSources(),
)

scalacOptions ++= Seq(
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-Ypartial-unification",
  "-Xlog-implicits",
  "-encoding",
  "utf8",
)