package com.canusi.os.common.util.cli

/**
 * Copyright: Canusi
 *
 * @author Benjamin Hargrave
 * @since 2019-11-19 
 */

import com.canusi.os.common.util.log.BasicLogger

import sys.process._

class BashManager {


  def run( path: String, parameters: String* ): Unit = {

    val scriptString = s"$path ${parameters.mkString(" ")}"
    val loggedOutput = BasicLogger( "bash-logger")


    loggedOutput.info( "Shell", s"Executing => $scriptString" )

    val stdout = new StringBuilder
    val stderr = new StringBuilder
    val logger = ProcessLogger(stdout append _, stderr append _)
    val status =  Seq( path ) ++ parameters ! logger

    val stdoutString = stdout.mkString
    val stderrString = stderr.mkString

    status match {
      case 0 =>
        loggedOutput.info("Shell", s"Success: Script with Params: ${scriptString}" +
          s". stdout: ${stdoutString} : stderr: ${stderrString} ")
      case _ =>
        loggedOutput.error("Shell Error", s"Failed: Script with Params: ${scriptString}" +
          s". stdout: ${stdoutString} : stderr: ${stderrString} ")
        throw new BashRuntimError(s"Failed: Script with Params: ${scriptString}" +
          s". stdout: ${stdoutString} : stderr: ${stderrString} ")
    }
  }

  private class BashRuntimError( script: String ) extends RuntimeException( script )

}


object BashManager {

  // ---------------------  Constructors

  def apply(): BashManager = {
    new BashManager()
  }

}
