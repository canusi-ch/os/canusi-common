package com.canusi.os.common.util.spawn

import java.io.{BufferedReader, File, FileInputStream, InputStreamReader}
import java.nio.charset.{Charset, CharsetDecoder, CodingErrorAction}

import com.canusi.os.common.util.caseclasswrapper.InputFilePath
import com.canusi.os.common.util.io.SimpleStream

import scala.io.Source
import scala.util.Try

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 09.01.20
 */


class FileStreamSpawner( val inputFilePath: InputFilePath ) extends Spawner[FileStreamSpawn] {


  override protected def buildSpawn(implicit self: FileStreamSpawner.this.type): FileStreamSpawn = {
    val decoder = Charset.forName("UTF-8").newDecoder()
    decoder.onMalformedInput(CodingErrorAction.IGNORE)
    val fileStream: FileInputStream = new FileInputStream(new File(inputFilePath.value  ))
    val streamReader: InputStreamReader = new InputStreamReader(fileStream, decoder)
    val reader: BufferedReader = new BufferedReader( streamReader )
    new FileStreamSpawn( fileStream, streamReader, reader ) {
      override protected val spawner: FileStreamSpawner = self
    }
  }


  override protected def close(): Unit = {
  }



}

abstract class FileStreamSpawn(   fileStream: FileInputStream
                                 , streamReader: InputStreamReader
                                 , val reader: BufferedReader )
  extends Spawn[String, FileStreamSpawner] with SimpleStream[String] {

  private var bufferFull: Boolean = false
  private var previousVal: String  = ""

  override protected def closeSpawn(): Unit = {
    Try( reader.close() )
    Try( streamReader.close() )
    Try( fileStream.close() )
  }

  override def close(): Unit = die()

  override def hasMore: Boolean =
    synchronized({
      if( !bufferFull ){
        previousVal = reader.readLine()
        bufferFull = true
      }
      previousVal != null
    }
  )

  override def getNext: String = {
    if( !hasMore ){
      throw new NoMoreElements( spawner.inputFilePath )
    }
    bufferFull = false
    previousVal
  }


  private class NoMoreElements( inputFilePath: InputFilePath ) extends RuntimeException( s"Buffered reader has no more elements to pull in from ${inputFilePath.value}")
}

object FileStreamSpawner {
  def apply(inputFilePath: InputFilePath): FileStreamSpawner = new FileStreamSpawner(inputFilePath)
}



