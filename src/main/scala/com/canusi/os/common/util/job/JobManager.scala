package com.canusi.os.common.util.job

import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Keep, Merge, Sink, Source}
import akka.stream.{ActorMaterializer, FlowShape, SourceShape}
import akka.util.Timeout
import com.canusi.os.common.util.job.AkkaStreamJob._

import scala.concurrent.{Future, Promise}

/**
 * Copyright: Canusi
 *
 * @author freelancer team
 * @since 2019-11-17
 *        Thanks to: Dmitry Openkov
 */


class JobManager()(implicit val materializer: ActorMaterializer) {

  def scheduleJob(akkaJob: AkkaStreamJob, params: JobSubmissionParameters): Future[Int] = {
    implicit val t = Timeout(params.jobTimeout)

    // building the Akka graph using the scalax.graph. Nodes represent jobs, edges - connections.
    // Each job is translated to Akka Flow and surrounded by a Merge (for multiple incoming connections)
    // and a Broadcast (for multiple outgoing connections).
    // For simplicity Merge and Broadcasts are used even if there's a plain path: node -> node -> node

    val graph = Source.fromGraph(
      GraphDSL.create() {
        implicit builder =>
          import GraphDSL.Implicits._
          val graph = createGraph(akkaJob.sources, akkaJob.sinks, akkaJob.jobs, akkaJob.bindings)
          validate(graph, akkaJob.sources, akkaJob.sinks, akkaJob.jobs)
              .foreach(error => throw new IllegalArgumentException(error))
          val allActualJobNodes = graph.nodes
              .filterNot(_.toOuter == AkkaStreamJob.startNode)
              .filterNot(_.toOuter == AkkaStreamJob.endNode)
          val jobFlows: Map[Job[_, _], FlowShape[Any, Any]] = allActualJobNodes
              .map(_.toOuter)
              .map(job => job -> builder.add(Flow.fromGraph(AkkaJobFlow[Any, Any](job.asInstanceOf[Job[Any, Any]]))))
              .toMap
          val merges = graph.nodes
              .filter(_.inDegree >= 1)
              .flatMap { node =>
                val merge = builder.add(Merge[Any](node.inDegree))
                node.incoming.map(_ -> merge)
              }
              .toMap
          val broadcasts = graph.nodes
              .filter(_.outDegree >= 1)
              .flatMap { node =>
                val broadcast = builder.add(Broadcast[Any](node.outDegree))
                node.outgoing.map(_ -> broadcast)
              }
              .toMap

          val graphStartNode = graph.get(startNode)
          val entryPoint = builder.add(Source.single(()))
          entryPoint ~> broadcasts(graphStartNode.outgoing.head)

          graph.edges.foreach { edge =>
            val broadcast = broadcasts(edge)
            val merge = merges(edge)
            broadcast ~> merge
          }
          allActualJobNodes.foreach { node =>
            val job = node.toOuter
            val flow = jobFlows(job)
            val merge = merges(node.incoming.head)
            val broadcast = broadcasts(node.outgoing.head)
            merge ~> flow ~> broadcast
          }

          val graphEndNode = graph.get(AkkaStreamJob.endNode)
          SourceShape(merges(graphEndNode.incoming.head).out)

      }

    )

    graph.toMat(Sink.fold(0) {
      case (count, _) => count + 1
    })(Keep.right).run()
  }

  def scheduleJob[Input, Output](job: Job[Input, Output], input: => Input, params: JobSubmissionParameters): Future[Output] = {
    val builder = AkkaStreamJob.getJobBuilder
    val source: SourceJob[Input] = _ => JobResult(input)
    val sink: ResultSink[Output] = new ResultSink[Output]
    builder.addSource(source)
    builder.addJob(job)
    builder.addSink(sink)
    builder.addJobBinding(source, job)
    builder.addJobBinding(job, sink)
    val akkaJob = builder.create().fold(err => throw new IllegalStateException("should not happen: " + err), identity)
    val future = scheduleJob(akkaJob, params)
    future.flatMap(_ => sink.result)(materializer.system.dispatcher)
  }

  private class ResultSink[O] extends SinkJob[O] {
    val promise: Promise[O] = Promise()
    val result: Future[O] = promise.future

    override def runNext(o: O) = JobResult(promise.success(o))
  }

}

object JobManager {
  def apply()(implicit materializer: ActorMaterializer): JobManager = new JobManager()
}
