package com.canusi.os.common.util.job

import com.canusi.os.common.util.log.{ RuntimeExceptionWrapper}

/**
 * Copyright: Canusi
 *
 * @author Benjamin Hargrave
 * @since 2019-11-22 
 */
object JobError {

    class FailedOnStart(exception: Throwable)
      extends RuntimeExceptionWrapper(s"Critical. Failed to start task: ", exception )
    class FailedOnShutDown( exception: Throwable)
      extends RuntimeExceptionWrapper(s"Critical. Failed to handle shutdown: ", exception )
    class FailedToHandleError(exception: Throwable)
      extends RuntimeExceptionWrapper(s"Critical. Failed to handle exception: ", exception )

}


