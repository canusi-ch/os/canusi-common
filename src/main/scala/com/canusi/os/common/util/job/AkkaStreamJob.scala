package com.canusi.os.common.util.job

import com.canusi.os.common.util.job.AkkaStreamJob.JobBinding
import scalax.collection.Graph
import scalax.collection.GraphEdge._
import scalax.collection.GraphPredef._

import scala.collection.mutable

/**
 * Copyright: Canusi
 * @author freelancer team
 * @since 2019-11-17
 * Thanks to: Dmitry Openkov
 */

object AkkaStreamJob {

  case class JobBinding[X](input: Job[_, X], output: Job[X, _], parallel: Boolean)

  class Builder {

    val sources = mutable.ListBuffer[SourceJob[Any]]()
    val sinks = mutable.ListBuffer[SinkJob[Any]]()
    val jobs = mutable.ListBuffer[Job[Any, Any]]()
    val bindings = mutable.ListBuffer[JobBinding[_]]()

    def addSource[O](job: SourceJob[O]): Unit = {
      sources += job
    }

    def addSink[I](job: SinkJob[I]) = {
      sinks += job.asInstanceOf[SinkJob[Any]]
    }

    def addJob[I, O](job: Job[I, O]) = {
      jobs += job.asInstanceOf[Job[Any, Any]]
    }

    def addJobBinding[I, X, O](input: Job[I, X], output: Job[X, O], parallel: Boolean = false) = {
      bindings += JobBinding(input, output, parallel)
    }

    def create(): Either[String, AkkaStreamJob] = {
      val graph = createGraph(sources, sinks, jobs, bindings)
      validate(graph, sources, sinks, jobs)
          .toLeft(AkkaStreamJob(sources.toList, sinks.toList, jobs.toList, bindings.toList))
    }
  }

  private[job] def createGraph(sources: Iterable[SourceJob[_]], sinks: Iterable[SinkJob[_]],
                          jobs: Iterable[Job[_, _]], bindings: Iterable[JobBinding[_]]): Graph[Job[_, _], DiEdge] = {
    val nodes = startNode :: endNode :: sources.toList ++ sinks.toList ++ jobs.toList
    val edges = bindings.map(b => b.input ~> b.output) ++ sources.map(startNode ~> _) ++ sinks.map(_ ~> endNode)
    Graph.from(nodes, edges)
  }

  def getJobBuilder: Builder = new Builder



  private[job] def validate(graph: Graph[Job[_, _], DiEdge], sources: Iterable[SourceJob[_]],
                            sinks: Iterable[SinkJob[_]], jobs: Iterable[Job[_, _]]): Option[String] = {
    val sourcesWithPredecessors = sources.map(j => j -> graph.get(j.asInstanceOf[Job[_, _]]).diPredecessors.size)
        .filter(_._2 > 1)
        .map(_._1)
    val sinksWithSuccessors = sinks.map(j => j -> graph.get(j).diSuccessors.size)
        .filter(_._2 > 1)
        .map(_._1)
    val unconnectedJobs = jobs.map(j => j -> graph.get(j).diSuccessors.size * graph.get(j).diPredecessors.size)
        .filter(_._2 == 0)
        .map(_._1)
    //we could replace the string error messages with error enums
    if (sources.isEmpty)
      Some("No sources")
    else if (sinks.isEmpty)
      Some("No sinks")
    else if (graph.isMulti)
      Some("Has multiple edges between same nodes")
    else if (graph.isCyclic)
      Some("Has cycles")
    else if (sourcesWithPredecessors.nonEmpty)
      Some("Sources with predecessors: " + sourcesWithPredecessors.map( _.getClass.getName ))
    else if (sinksWithSuccessors.nonEmpty)
      Some("Sinks with successors: " + sinksWithSuccessors.map( _.getClass.getName ))
    else if (unconnectedJobs.nonEmpty)
      Some("Jobs are missing bind connections: " + unconnectedJobs.map( _.getClass.getName ))
    else if (!graph.isConnected)
      Some("Not connected")
    else
      None
  }

  private[job] class NoOp extends Job[Any, Any] {
    override def runNext(input: Any) = {}

    override def onFail(exception: Throwable, input: Any) = {}

    override def preTask(): Unit = {}

    override def finallyMethod(): Unit = {}
  }
  private[job] val startNode = new NoOp()
  private[job] val endNode = new NoOp()

}

case class AkkaStreamJob(sources: List[SourceJob[_]],
                         sinks: List[SinkJob[_]],
                         jobs: List[Job[_, _]],
                         bindings: List[JobBinding[_]],
                        ) {

}
