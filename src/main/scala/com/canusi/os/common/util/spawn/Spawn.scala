package com.canusi.os.common.util.spawn

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 09.01.20
 */


abstract class Spawn[+Output, +Parent <: Spawner[_]] {

  protected val spawner: Parent

  protected def closeSpawn(): Unit

  def die(): Unit = {
    spawner.receiveCloseFromSpawn( this )
    closeSpawn()
  }


}