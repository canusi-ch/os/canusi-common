package com.canusi.os.common.util.log

import com.typesafe.scalalogging.Logger

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 2020-04-09 10:35
 *
 */
trait AbstractLogger {

  def info( message: String, description: String ): Unit
  def debug( message: String, description: String ): Unit
  def warning( message: String, description: String ): Unit
  def error( message: String, description: String ): Unit

}

