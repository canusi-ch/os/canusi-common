package com.canusi.os.common.util.context

import scala.collection.mutable
import scala.reflect.runtime.universe.{TypeTag, typeTag}


/**
 * Copyright: Canusi
 *
 * @author freelancer team
 * @since 2019-11-17
 * @version 2
 * @author Benjamin Hargrave
 * Added small changes to make easier handle context
 */

trait ContextObjectRegistry {
  import ContextObjectRegistry._

  private val parentContexts = mutable.ListBuffer[ContextObjectRegistry]()
  private val internal = mutable.ListBuffer[(TypeTag[_], Any)]()

  def contextAddChild[ChildType <: ContextObjectRegistry : TypeTag](child: ChildType): ChildType = {
    val tags = getContextTags
    child.internal.find(t => tags.exists(_.tpe =:= t._1.tpe)) match {
      case None => child.parentContexts += this
      case Some(_) => throw new AlreadyInParentContextException(typeTag[ChildType])
    }
    child
  }

  final def contextAddComponent[ComponentType: TypeTag](value: ComponentType): ComponentType = {
    val typeTg = typeTag[ComponentType]
    lookupInContext[ComponentType] match {
      case None => internal += typeTg -> value
      case Some(_) => throw new AlreadyInContextException(typeTg)
    }
    value
  }

  def contextGetLazyComponent[T: TypeTag]: T = {
    lookupInContext[T] match {
      case Some(value) => value
      case None => throw new NotInContextException(typeTag[T])
    }
  }

  private def lookupInContext[T: TypeTag]: Option[T] = {
    val typeTg = typeTag[T]
    internal.find(_._1.tpe =:= typeTg.tpe).map { value =>
      value._2.asInstanceOf[T]
    }.orElse {
      parentContexts
        .find(_.lookupInContext[T].isDefined)
        .flatMap(_.lookupInContext[T])
    }
  }

  private def getContextTags: List[TypeTag[_]] = {
    parentContexts.flatMap(_.getContextTags).toList ++ internal.map(_._1).toList
  }
}

object ContextObjectRegistry {
  class AlreadyInContextException(t: TypeTag[_]) extends RuntimeException(s"Element of type ${t.tpe.typeSymbol} is already present in context registry")
  class AlreadyInParentContextException(t: TypeTag[_]) extends RuntimeException(s"Element of type ${t.tpe.typeSymbol} is already present in parent context registry")
  class NotInContextException(t: TypeTag[_]) extends RuntimeException(s"No Element of type ${t.tpe.typeSymbol} in context")
}