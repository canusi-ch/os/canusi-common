package com.canusi.os.common.util.log

import akka.actor.{ActorRef, ActorSystem, Props}
import com.canusi.os.common.util.actor.io.ActorIoFile
import com.canusi.os.common.util.caseclasswrapper.{OutputDirPath, OutputFilePath}

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 07.01.20
 */


object FileLogger{

  private val logAllType = "ALL"
  private val logInfoType = "INFO"
  private val logErrorType = "ERROR"
  private val logWarningType = "WARNING"
  private val logDebugType = "DEBUG"

  def apply(name: String, baseFilePathOutput: OutputFilePath, actorSystem: ActorSystem): FileLogger = new FileLogger(name, baseFilePathOutput, actorSystem)


  class FileLogger(name: String, baseFilePathOutput: OutputFilePath, actorSystem: ActorSystem ) extends BasicLogger( name ) {

    private val loggerName = s"${name}-filelogger"

    val mainFileActor: ActorRef = getActorRef( logAllType )
    val debugFileActor: ActorRef = getActorRef( logDebugType )
    val infoFileActor: ActorRef =  getActorRef(logInfoType )
    val warningFileActor: ActorRef =  getActorRef( logWarningType )
    val errorFileActor: ActorRef = getActorRef( logErrorType)


    private def getActorRef( logType: String ): ActorRef = {
      actorSystem.actorOf( Props( new ActorIoFile( OutputFilePath( s"${baseFilePathOutput.value}-${logType}.log" ) ) ), name = s"actor-log-${logType}-${loggerName}")
    }


    override def info( message: String, description: String ): Unit = {
      infoFileActor !  buildLogMessage( logInfoType, message, description )
      mainFileActor !  buildLogMessage( logInfoType, message, description )
      super.info( message, description )
    }

    override def debug( message: String, description: String, args: Any* ): Unit = {
      debugFileActor !  buildLogMessage( logDebugType, message, description )
      mainFileActor !  buildLogMessage( logDebugType, message, description )
      super.debug( message, description, args: _* )
    }

    override def warning( message: String, description: String, handler: (String) => Unit = String => {}   ): Unit = {
      mainFileActor !  buildLogMessage( logWarningType, message, description )
      mainFileActor !  buildLogMessage( logWarningType, message, description )
      super.warning( message, description, handler )
    }

    override def error( message: String, description: String, errorHandler: (String) => Unit  ): Unit = {
      errorFileActor !  buildLogMessage( logErrorType, message, description )
      mainFileActor !  buildLogMessage( logErrorType, message, description )
      super.error( message, description,  errorHandler )
    }

    private def buildLogMessage( logType: String, message: String, description: String ): String = {
      s"$loggerName $logType: ${message}: ${description}"
    }

  }

}


