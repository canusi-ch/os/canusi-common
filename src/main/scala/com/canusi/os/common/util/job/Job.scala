package com.canusi.os.common.util.job


import java.time.LocalDateTime

import com.canusi.os.common.util.Loggers
import com.canusi.os.common.util.log.{BasicLogger, ErrorHandler}

import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.util.{Failure, Success, Try}

/**
 * Copyright: Canusi
 * @author freelancer team
 * @since 2019-11-17
 * Thanks to: Dmitry Openkov
 */

case class JobResult[+T](value: T, hasMore: Boolean)

object JobResult {
  def apply[T](value: T): JobResult[T] = new JobResult(value, false)
}

trait Job[-Input, +Output] {

  implicit def singleResult[T](value: T) = JobResult(value, hasMore = false)

  def runNext(input: Input): JobResult[Output]

  def onFail(exception: Throwable, input: Input): JobResult[Output] = {
    Loggers.Job.error( s"${this.getClass.getName} Failed: Input:${input.toString}"
      ,s"${ErrorHandler.convert2String( exception )}" )
    throw exception
  }

  def preTask(): Unit = {}

  def finallyMethod(): Unit = {}


  private[job] final def tryRunNext(input: Input): JobResult[Output] = {
    Try( runNext(input) ) match {
      case Success(value) => value
      case Failure(exception) =>
        tryOnFail( exception, input )
    }
  }

  private[job]final def tryOnFail(exception: Throwable, input: Input): JobResult[Output] = {
    Try( onFail( exception, input  )) match {
      case Success( value ) => value
      case Failure( exception ) =>
        throw new JobError.FailedToHandleError( exception )
    }
  }

  private[job] final def tryPreTask(): Unit = {
    Try( preTask() ) match {
      case Success(_) =>
      case Failure( exception ) => throw new JobError.FailedOnStart( exception )
    }
  }

  private[job] final def tryFinallyMethod(): Unit = {
    Try( finallyMethod() ) match {
      case Success(_) =>
      case Failure( exception ) => throw new JobError.FailedOnShutDown( exception )
    }
  }
}




trait SourceJob[+Output] extends Job[Unit, Output] {
}

trait SinkJob[-Input] extends Job[Input, Unit] {
}


case class JobSubmissionParameters(
                                    //all paths in the graph go in parallel
                                    parallel: Boolean = false,
                                    // the lower the more important
                                    priority: Int = 1000,
                                    jobTimeout: FiniteDuration,
                                    //if the job is not started within this time frame the priority is maximized
                                    jobTimeFrame: Duration = Duration.Inf,
                                    schedule: JobSchedule = JobSchedule(),
                                  )

case class JobSchedule(
                        // if set to false, job will only run 1x
                        flagAllowJobRestart: Boolean = false,
                        // if true, restart job can execute job even if previous is still running
                        flagAllowMultipleInstances: Boolean = false,
                        // restart job on this interval period
                        timeRestartIntervalMS: Int = 60 * 60 * 1000,
                        startSchedule: LocalDateTime = LocalDateTime.now(),
                        stopSchedule: LocalDateTime = LocalDateTime.MAX,
                      )