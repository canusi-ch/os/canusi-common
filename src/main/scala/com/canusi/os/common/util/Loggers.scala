package com.canusi.os.common.util

import com.canusi.os.common.util.log.BasicLogger

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 31.12.19
 */

private[util] object Loggers {

  val Job = BasicLogger( "common-job" )


}
