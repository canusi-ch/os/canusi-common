package com.canusi.os.common.util.log

/**
 * Copyright: Canusi
 *
 * @author Benjamin Hargrave
 * @since 2019-11-22 
 */

class RuntimeExceptionWrapper( context: String, exception: Throwable ) extends RuntimeException{
  override def getMessage: String = {
    context + " -> " + exception.getMessage
  }
  override def getCause: Throwable = exception.getCause
  override def getStackTrace: Array[StackTraceElement] = exception.getStackTrace
  override def printStackTrace(): Unit = exception.printStackTrace()

}