package com.canusi.os.common.util.job

import akka.stream.stage._
import akka.stream.{Attributes, FlowShape, Inlet, Outlet}

/**
 * Copyright: Canusi
 *
 * @author Benjamin Hargrave
 * @since 2019-11-22 
 */

object AkkaJobFlow {

  def apply[Input,Output](job: Job[Input,Output]): AkkaJobFlow[Input,Output] = new AkkaJobFlow(job)

}


class AkkaJobFlow[Input,Output](job: Job[Input, Output]) extends GraphStage[FlowShape[Input, Output]] {

  private val in: Inlet[Input] = Inlet[Input]("Flow.in")
  private val out: Outlet[Output] = Outlet[Output]("Flow.out")
  val shape = FlowShape.of( in, out )

  override def createLogic(attr: Attributes): GraphStageLogic =
    new GraphStageLogicWithLogging(shape){

      private var lastInput: Input = _
      private var hasMore = false

      override def preStart(): Unit = {
        super.preStart()
        job.tryPreTask()
      }

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          executeJob(grab(in))
        }

        override def onUpstreamFinish(): Unit = {
          while (hasMore) executeJob(lastInput, viaEmit = true)
          complete(out)
        }
      })

      setHandler( out , new OutHandler {
        override def onPull(): Unit = {
          if (hasMore)
            executeJob(lastInput)
          else
            pull(in)
        }
      })

      private def executeJob(input: Input, viaEmit: Boolean = false): Unit = {
        lastInput = input
        val jobResult: JobResult[Output] = job.tryRunNext(lastInput)
        hasMore = jobResult.hasMore
        if (viaEmit) {
          emit(out, jobResult.value)
        } else {
          push(out, jobResult.value)
        }
      }


      override def postStop(): Unit = {
        job.tryFinallyMethod()
        super.postStop()
      }
    }

}