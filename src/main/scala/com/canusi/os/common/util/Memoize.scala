package com.canusi.os.common.util

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 07.01.20
 */

final class Memoize[R] {

  // Cached function call results.
  private var value:  Option[R] = None

  def set[V <: R]( newVal: V ): V = synchronized {
    require( value == None
      , s"You cannot set a Memoize value 2x. Found${newVal}, pre-existing value of ${this.value}" )
    value = Some( newVal )
    newVal
  }

  def apply(): Option[R] = value
}


object Memoize {

  def apply[R]() = new Memoize[R]()
}
