package com.canusi.os.common.util.io

import scala.util.{Failure, Success, Try}

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 09.01.20
 */

object StreamConsumer {

  def closeOnComplete[T]( stream: SimpleStream[_], f: => T): T = {
    val result = Try(f)
    stream.close()
    result match {
      case Success(value) => value
      case Failure(exception) => throw exception
    }
  }

}
