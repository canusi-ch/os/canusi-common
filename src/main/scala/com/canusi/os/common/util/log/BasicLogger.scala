package com.canusi.os.common.util.log

import com.typesafe.scalalogging.Logger

/**
 * Copyright: Canusi
 *
 * @author Benjamin Hargrave
 * @since 2019-11-18 
 */


class BasicLogger( name: String ) {


  private val LOGGER = Logger( name )


  def info( message: String, description: String ): Unit = {
    LOGGER.info( s"${message}: ${description}")
  }
  def debug( message: String, description: String, args: Any* ): Unit = {
    LOGGER.debug( s"${message}: ${description} --> ${args.mkString(":")}" )
  }

  def warning( message: String, description: String,  handler: String => Unit = ( errorMsg: String  ) => {}   ): Unit = {
    LOGGER.warn( s"${message}: ${description}")
    handler( s"${message}: ${description}" )
  }

  def error( message: String, description: String, handler: String => Unit = ( errorMsg: String  ) => {}   ): Unit = {
    LOGGER.error( s"${message}: ${description}")
    handler( s"${message}: ${description}" )
  }

}

object BasicLogger {
  def apply(name: String): BasicLogger = new BasicLogger(name)
}

