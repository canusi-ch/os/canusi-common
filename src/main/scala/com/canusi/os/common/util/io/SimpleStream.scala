package com.canusi.os.common.util.io

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 09.01.20
 */

trait SimpleStream[+T] {

  def hasMore(): Boolean
  def getNext(): T
  def close()

}
