package com.canusi.os.common.util.service

import com.canusi.os.common.util.log.BasicLogger

import scala.reflect.ClassTag
import scala.reflect.runtime.universe._

/**
 * Copyright: Canusi
 *
 * @author freelancer team
 * @since 2019-12-25
 *        Thanks to: Dmitry Openkov
 */
trait Persistence {
  private val logger = new BasicLogger("PersistentConnector")

  def save(key: String, value: Any)

  def read[T: ClassTag](key: String): Option[T]

}

