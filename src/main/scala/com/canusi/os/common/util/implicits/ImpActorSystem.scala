package com.canusi.os.common.util.implicits

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.typesafe.config.{Config}

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 07.01.20
 */

trait ImpActorSystem {
    val actorSystemName: String
    protected val config: Config
    // setup implicits for other actors
    lazy implicit val system: ActorSystem = ActorSystem( actorSystemName, config )
    lazy implicit val materializer: ActorMaterializer = ActorMaterializer()
    lazy implicit val ec = system.dispatcher


    def shutDown(): Unit ={
        system.terminate()
    }
}
