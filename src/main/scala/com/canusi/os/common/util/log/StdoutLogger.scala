package com.canusi.os.common.util.log

import com.typesafe.scalalogging.Logger

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 2020-04-09 15:46
 *
 *
 *        This class is necessary because: the existing basic logger is a bad name and would take too long to refactor
 *        This class supports the business purpose: provides a simple logger to the output
 *
 */

class StdoutLogger( name: String ) extends AbstractLogger {

  private val LOGGER = Logger( name )
  override def info(message: String, description: String): Unit = {
    LOGGER.info( formatString( message, description ))
  }

  override def debug(message: String, description: String): Unit = {
    LOGGER.debug( formatString( message, description ))
  }

  override def warning(message: String, description: String): Unit = {
    LOGGER.warn( formatString( message, description ))
  }

  override def error(message: String, description: String): Unit = {
    LOGGER.error( formatString( message, description ))
  }


  private def formatString( message: String , description: String ): String = {
      s"Message: ${message } - Description: ${description}"
  }


}

object StdoutLogger {

  def apply(name: String): StdoutLogger = new StdoutLogger(name)

}


