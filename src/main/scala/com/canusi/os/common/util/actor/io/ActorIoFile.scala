package com.canusi.os.common.util.actor.io

import java.io.PrintWriter

import akka.actor.Actor
import com.canusi.os.common.util.caseclasswrapper.OutputFilePath

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 07.01.20
 */

object ActorIoFile {

  def apply(outFile: OutputFilePath): ActorIoFile = new ActorIoFile(outFile)

}


class ActorIoFile(outFile: OutputFilePath) extends Actor {




  val writer = new PrintWriter(outFile.value)

  def receive = {
    case str: String => writer.write(str);
  }

  override def postStop() = {
    writer.flush()
    writer.close()
  }

}
