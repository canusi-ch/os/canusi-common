package com.canusi.os.common.util.job

import com.canusi.os.common.util.Loggers
import com.canusi.os.common.util.job.AkkaStreamJob.JobBinding

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 31.12.19
 */

object JobBuilder {

  def apply(): JobBuilder = new JobBuilder()

  class JobBuilder {
    private val jobBuilder = AkkaStreamJob.getJobBuilder
    private val source = new SingleSource()
    private val target = new SingleTarget()


    jobBuilder.addSink( target )


    private var isFirst: Boolean = true
    private var lastBinding: JobBinding[Unit] = null
    private var previousJob: Job[_,_] = null

    def add[Input,Output]( job: Job[Input, Output]): JobBuilder = {
      jobBuilder.addJob( job )
      isFirst match {
        case true =>
          jobBuilder.addSource( source  )
          job match {
            case candidateFirstJob: Job[Unit, _] => jobBuilder.addJobBinding( source, candidateFirstJob )
            case _ => throw new IllegalArgumentException( s"First job ${job.getClass.getName} must be Source job or have type [Unit,_]")
          }
          isFirst = false

        case false =>
          previousJob match {
            case previousValidJob: Job[Any , Input] => {
              jobBuilder.addJobBinding[Any, Input, Output](
                previousValidJob, job )
            }
            case previousInvalidJob: Job[_, _] => throw new IllegalArgumentException(
              s"Cannot bind previous job ${previousInvalidJob.getClass.getName} to ${job.getClass.getName} as Input/Outputs do not match"
            )
          }
      }

      // save potential last candidate
      job match {
        case candidateLastJob: Job[_, Unit] => lastBinding = JobBinding( candidateLastJob, target, parallel = false )
        case _ => lastBinding = null
      }

      previousJob = job
      this
    }

    // todo not so happy with the build function being called multiple times. Anyhoo
    private var akkaStreamJob: AkkaStreamJob = null
    def build(): AkkaStreamJob = {
      akkaStreamJob match {
        case null => require( lastBinding != null, "Last job applied needs to be output of Unit, or a Sink Job" )
          jobBuilder.bindings += lastBinding
          jobBuilder.create() match {
            case Left(error) =>
              Loggers
                .Job
                .error( "Failed to build job"
                  , jobBuilder.jobs.mkString("\n")
                )
              throw new IllegalStateException( error )

            case Right(graph) => {
              akkaStreamJob = graph
              graph
            }
          }
        case value: AkkaStreamJob => value
      }
    }
  }


  private class SingleSource extends SourceJob[Unit]{
    override def runNext(input: Unit): JobResult[Unit] = {}
  }

  private class SingleTarget extends SinkJob[Unit]{
    override def runNext(input: Unit): JobResult[Unit] = {}
  }

}
