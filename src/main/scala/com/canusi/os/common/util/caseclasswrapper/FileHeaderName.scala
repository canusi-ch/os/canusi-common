package com.canusi.os.common.util.caseclasswrapper

/**
 * Copyright: Canusi
 *
 * @author Benjamin Hargrave
 * @since 2019-11-24 
 */
case class FileHeaderName( value: String ) extends AnyVal
