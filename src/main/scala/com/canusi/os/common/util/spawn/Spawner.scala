package com.canusi.os.common.util.spawn

import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 09.01.20
 */

trait Spawner[T] {

  private val spawnList: ListBuffer[T] = ListBuffer[T]()

  protected[spawn] def receiveCloseFromSpawn( spawn: Spawn[_,_] ): Unit =  {
    Try( {
      val indexPos = spawnList.indexOf( spawn )
      spawnList.remove( indexPos )
    } ) match {
      case Success(value) => if( spawnList.isEmpty ) close()
      case Failure(exception) => throw new IllegalSpawnContextAccess( spawn )
    }
  }

  protected def close(): Unit

  protected def buildSpawn( implicit spawner: this.type ): T

  def spawn(): T = {
    val next = buildSpawn( this )
    spawnList += next
    next
  }


  private class IllegalSpawnContextAccess( spawn: Spawn[_, _] ) extends RuntimeException( s"Spawn ${spawn} is not in Spawner context - this was either already removed, or is calling a Spawner that didn't birth it")
}


