package com.canusi.os.common.context

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit}
import com.canusi.os.common.util.context.ContextObjectRegistry
import com.canusi.os.common.util.context.ContextObjectRegistry._
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}
import org.specs2.mutable.Specification


/**
 * Copyright: Canusi
 * @author freelancer team
 * @since 2019-11-17
 * Thanks to: Dmitry Openkov
 */

class ContextObjectRegistrySpecification extends TestKit(ActorSystem("ContextObjectRegistrySpecs") )
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {


  implicit val materializer = ActorMaterializer()

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val DriverJosh = Driver("Josh")

  "ContextObjectRegistry addComponentToContext" should {
    "add components to registry" in {
      val seat = DriverSeat(DriverJosh)
      seat.contextGetLazyComponent[Driver] mustEqual  DriverJosh
    }

    "throw error when adding 2 components of the same type" in {
      assertThrows[AlreadyInContextException] {
        val seat = DriverSeat(DriverJosh)
        seat.contextAddComponent(Driver("Jon"))
      }
    }

    "throw error when grand parent has registered a component of the same type" in {
      val seat = DriverSeat(DriverJosh)
      val car = Car(seat)
      val garage = Garage(car)
      garage.contextAddComponent(Color(29))
      assertThrows[AlreadyInContextException] {
        seat.contextAddComponent(Color(42))
      }
    }
  }

  "ContextObjectRegistry getSingletonObject" should {

    "throw an `NotInContextException` when no component of the given type is registered" in {
      val seat = DriverSeat(DriverJosh)
      assertThrows[NotInContextException]{
        seat.contextGetLazyComponent[String]
      }
      assertThrows[NotInContextException]{
        seat.contextGetLazyComponent[Color]
      }
    }

    "return component from parent's registry" in {
      val seat = DriverSeat(DriverJosh)
      val car = Car(seat)

      assertThrows[NotInContextException]{
        seat.contextGetLazyComponent[Color]
      }

      val color = Color(23)
      car.contextAddComponent(color)
      seat.contextGetLazyComponent[Color] mustEqual  color
    }

    "NOT return component from child's context" in {
      val seat = DriverSeat(DriverJosh)
      val car = Car(seat)

      seat.contextAddComponent(Color(23))
      assertThrows[NotInContextException]{
        car.contextGetLazyComponent[Color]
      }
    }

    "retrieve typed collections from context" in {
      val seat = DriverSeat(DriverJosh)
      seat.contextAddComponent(List(1, 2, 3))
      seat.contextAddComponent(List("a", "b"))
      seat.contextGetLazyComponent[List[Int]] must equal ( List(1, 2, 3) )
      seat.contextGetLazyComponent[List[String]] must equal ( List("a", "b") )
    }

    "NOT return components of a subtype class" in {
      val seat = DriverSeat(DriverJosh)
      seat.contextAddComponent(FuzzyColor(22))
       assertThrows[NotInContextException]{( seat.contextGetLazyComponent[Color] ) }
    }
  }

  "ContextObjectRegistry shareContextToChild" should {
    "throw an error if child has a component of the current context" in {
      val seat = DriverSeat(DriverJosh)
      seat.contextAddComponent(Color(23))
      val grandParent = GrandParent()
      grandParent.contextAddComponent(Color(11))
      grandParent.contextAddComponent(seat)
      assertThrows[AlreadyInParentContextException] { grandParent.contextAddChild(seat) }

    }
  }
}


class Color(num: Int)
object Color {
  def apply(num: Int): Color = new Color(num)
}

case class FuzzyColor(num: Int) extends Color(num * 3)

case class Driver(name: String)
case class DriverSeat(driver: Driver) extends ContextObjectRegistry {
  contextAddComponent(driver)
}

case class Car(driverSeat: DriverSeat) extends ContextObjectRegistry {
  contextAddComponent(driverSeat)
  contextAddChild(driverSeat)
}
case class Garage(car: Car) extends ContextObjectRegistry {
  contextAddComponent(car)
  contextAddChild(car)
}
case class GrandParent() extends ContextObjectRegistry {}
