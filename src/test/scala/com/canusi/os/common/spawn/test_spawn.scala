package com.canusi.os.common.spawn

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit}
import com.canusi.os.common.util.caseclasswrapper.InputFilePath
import com.canusi.os.common.util.spawn.FileStreamSpawner
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}

import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
 * Copyright Canusi
 *
 * @author Benjamin Hargrave
 * @since 09.01.20
 */

class test_spawn extends TestKit(ActorSystem("BasicSpec"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {

  implicit val materializer = ActorMaterializer()

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "FileSpawner" should { " spawn an object " in {

    val file = getClass().getResource( "/spawn_files/test.path").getPath

    val spawner = FileStreamSpawner( InputFilePath( file) )
    val spawn = spawner.spawn()

    val listOfValues = ListBuffer[String]()
    while( spawn.hasMore ) {
      listOfValues += spawn.getNext
    }

    listOfValues must equal( ListBuffer( "value1", "value2", "value3" ) )

    spawn.die()
   }
  }


  it should { "close the parent object, once the child is closed" in {

    val file = getClass().getResource( "/spawn_files/test.path").getPath

    val spawner = FileStreamSpawner( InputFilePath( file) )
    val spawn = spawner.spawn()

    var listOfValues = ListBuffer[String]()
    while( spawn.hasMore ) {
      listOfValues += spawn.getNext
    }

    spawn.die()
    // No longer part of the design
    // assertThrows[java.io.IOException]( spawner.spawn().getNext ) // should throw an error
  }
  }
}
