package com.canusi.os.common.job

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit}
import com.canusi.os.common.util.job.JobError.FailedToHandleError
import com.canusi.os.common.util.job.{AkkaStreamJob, Job, JobManager, JobSubmissionParameters, SinkJob, SourceJob}
import org.scalatest.concurrent.{PatienceConfiguration, ScalaFutures}
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}

import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Copyright: Canusi
 * @author freelancer team
 * @since 2019-11-17
 * Thanks to: Dmitry Openkov
 */

class test_NegativeSpec extends TestKit(ActorSystem("NegativeSpec"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {

  implicit val materializer = ActorMaterializer()

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "JobManager" should {
    "return a failed future in case of an error in a plain job" in {
      val failing = new FailingJob
      val failedResult = JobManager().scheduleJob(failing, "I consist of 5 words",
        JobSubmissionParameters(jobTimeout = 2 seconds))
      ScalaFutures.whenReady(failedResult.failed, PatienceConfiguration.Timeout(3 seconds)) { e =>
        e mustBe a [FailedToHandleError]
      }
    }
    "return failed Future for failing job and return the result for a succeeded job" in {
      val (_, failingJob: AkkaStreamJob) = createJob(false)
      val (consoleSink, succeededJob: AkkaStreamJob) = createJob(true)
      val failedResult = JobManager().scheduleJob(failingJob, JobSubmissionParameters(jobTimeout = 2 seconds))
      val result = JobManager().scheduleJob(succeededJob, JobSubmissionParameters(jobTimeout = 2 seconds))
      Await.result(result, 2 seconds) must be(1)
      consoleSink.messages.size must be(1)
      consoleSink.messages must contain(7)
      ScalaFutures.whenReady(failedResult.failed, PatienceConfiguration.Timeout(3 seconds)) { e =>
        e mustBe a [FailedToHandleError]
      }
    }

    "get result out of the onFail method in case of a failure" in {
      val textSource = new TextSource("Simple text for processing by the jobs")
      val fallbackJob = new FallbackJob
      val (consoleSink, job: AkkaStreamJob) = createChainJob(textSource, new ConsoleJob[Int]("sink1"), fallbackJob)
      val result = JobManager().scheduleJob(job, JobSubmissionParameters(jobTimeout = 2 seconds))
      Await.result(result, 2 seconds) must be(1)
      consoleSink.messages.size must be(1)
      consoleSink.messages must contain(7)
    }
  }

  private def createJob(success: Boolean): (ConsoleJob[Int], AkkaStreamJob) = {
    val textSource = new TextSource("Simple text for processing by the jobs")
    val consoleSink = new ConsoleJob[Int]("sink")
    if (success)
      createChainJob(textSource, consoleSink, new WordCountJob)
    else {
      createChainJob(textSource, consoleSink, new FailingJob)
    }
  }

  private def createChainJob[I, O, Sink <: SinkJob[O]](sourceJob: SourceJob[I], sinkJob: Sink,
                                                       job: Job[I, O]): (Sink, AkkaStreamJob) = {
    val builder = AkkaStreamJob.getJobBuilder
    builder.addSource(sourceJob)
    builder.addSink(sinkJob)
    builder.addJob(job)
    builder.addJobBinding(sourceJob, job)
    builder.addJobBinding(job, sinkJob)
    val akkaJob = builder.create().getOrElse(throw new IllegalStateException("wrong graph"))
    (sinkJob, akkaJob)
  }

}

















