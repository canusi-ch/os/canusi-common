package com.canusi.os.common.job

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit}
import com.canusi.os.common.util.job.{AkkaStreamJob, Job, JobManager, JobSubmissionParameters, SinkJob, SourceJob}
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}

import scala.collection.mutable
import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Copyright: Canusi
 * @author freelancer team
 * @since 2019-11-17
 * Thanks to: Dmitry Openkov
 */

class BasicSpec extends TestKit(ActorSystem("BasicSpec"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {

  implicit val materializer = ActorMaterializer()

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "JobManager" should {
    "execute a plain job and return the result" in {
      val countJob = new WordCountJob
      val numWords = JobManager().scheduleJob(countJob, "I consist of 5 words",
        JobSubmissionParameters(jobTimeout = 2 seconds))
      Await.result(numWords, 2 seconds) must be(5)
    }
    "execute chain job and return number of followed messages" in {
      val builder = AkkaStreamJob.getJobBuilder
      val textSource = new TextSource("Simple text for processing by the jobs")
      val countJob = new WordCountJob
      val squareJob = new SquareJob
      val consoleSink = new ConsoleJob[Int]("sink")
      builder.addSource(textSource)
      builder.addSink(consoleSink)
      builder.addJob(countJob)
      builder.addJob(squareJob)
      builder.addJobBinding(textSource, countJob)
      builder.addJobBinding(countJob, squareJob)
      builder.addJobBinding(squareJob, consoleSink)
      val job = builder.create().getOrElse(throw new IllegalStateException("wrong graph"))
      val numMessages = JobManager().scheduleJob(job, JobSubmissionParameters(jobTimeout = 2 seconds))
      Await.result(numMessages, 2 seconds) must be(1)
      consoleSink.messages.size must be(1)
      consoleSink.messages must contain(49)
    }

    "execute 2 input 1 output job" in {
      val builder = AkkaStreamJob.getJobBuilder
      val textSource1 = new TextSource("Simple text for processing by the jobs")
      val textSource2 = new TextSource("A shorter text")
      val countJob = new WordCountJob
      val squareJob = new SquareJob
      val consoleSink = new ConsoleJob[Int]("sink")
      builder.addSource(textSource1)
      builder.addSource(textSource2)
      builder.addSink(consoleSink)
      builder.addJob(countJob)
      builder.addJob(squareJob)
      builder.addJobBinding(textSource1, countJob)
      builder.addJobBinding(textSource2, countJob)
      builder.addJobBinding(countJob, squareJob)
      builder.addJobBinding(squareJob, consoleSink)
      val job = builder.create().getOrElse(throw new IllegalStateException("wrong graph"))
      val numMessages = JobManager().scheduleJob(job, JobSubmissionParameters(jobTimeout = 2 seconds))
      Await.result(numMessages, 2 seconds) must be(2)
      consoleSink.messages.size must be(2)
      consoleSink.messages must contain allOf(9, 49)
    }

    "execute 2 input 3 output job" in {
      val builder = AkkaStreamJob.getJobBuilder
      val textSource1 = new TextSource("Simple text for processing by the jobs")
      val textSource2 = new TextSource("A shorter text")
      val countJob = new WordCountJob
      val squareJob = new SquareJob
      val multiplyJob = new MultiplyJob(10)
      val consoleSink1 = new ConsoleJob[Int]("sink1")
      val consoleSink2 = new ConsoleJob[Int]("sink2")
      val consoleSink3 = new ConsoleJob[Int]("sink3")
      builder.addSource(textSource1)
      builder.addSource(textSource2)
      builder.addSink(consoleSink1)
      builder.addSink(consoleSink2)
      builder.addSink(consoleSink3)
      builder.addJob(countJob)
      builder.addJob(squareJob)
      builder.addJob(multiplyJob)
      builder.addJobBinding(textSource1, countJob)
      builder.addJobBinding(textSource2, countJob)
      builder.addJobBinding(countJob, squareJob)
      builder.addJobBinding(countJob, multiplyJob)
      builder.addJobBinding(squareJob, multiplyJob)
      builder.addJobBinding(squareJob, consoleSink1)
      builder.addJobBinding(multiplyJob, consoleSink2)
      builder.addJobBinding(squareJob, consoleSink3)
      builder.addJobBinding(countJob, consoleSink3)
      val job = builder.create().getOrElse(throw new IllegalStateException("wrong graph"))
      val numMessages = JobManager().scheduleJob(job, JobSubmissionParameters(jobTimeout = 2 seconds))
      Await.result(numMessages, 2 seconds) must be(10)
      consoleSink1.messages.size must be(2)
      consoleSink1.messages must contain allOf(9, 49)
      consoleSink2.messages.size must be(4)
      consoleSink2.messages must contain allOf(90, 490, 30, 70)
      consoleSink3.messages.size must be(4)
      consoleSink3.messages must contain allOf(9, 49, 3, 7)
    }
    "execute source ~> sink (2 jobs only) and return number of followed messages" in {
      val builder = AkkaStreamJob.getJobBuilder
      val textSource = new TextSource("msg_1")
      val consoleSink = new ConsoleJob[String]("sink")
      builder.addSource(textSource)
      builder.addSink(consoleSink)
      builder.addJobBinding(textSource, consoleSink)
      val job = builder.create().fold(err => throw new IllegalStateException("wrong graph: " + err), identity)
      val numMessages = JobManager().scheduleJob(job, JobSubmissionParameters(jobTimeout = 2 seconds))
      Await.result(numMessages, 2 seconds) must be(1)
      consoleSink.messages.size must be(1)
      consoleSink.messages must contain("msg_1")
    }
  }
}


class TextSource(text: String) extends SourceJob[String] {
  Thread.sleep(1000)
  override def runNext(input: Unit) = text
}

class WordCountJob(val sleep: Int = 0) extends Job[String, Int] {
  override def runNext(input: String) = {
    Thread.sleep(sleep)
    input.split("\\s+").length
  }
}

class SquareJob(val sleep: Int = 0) extends Job[Int, Int] {
  override def runNext(input: Int) = {
    Thread.sleep(sleep)
    input * input
  }
}

class MultiplyJob(m: Int, sleep: Int = 0) extends Job[Int, Int] {
  override def runNext(input: Int) = {
    Thread.sleep(sleep)
    input * m
  }
}

class NoOp extends Job[Unit, Unit] {
  override def runNext(input: Unit) = {}
}

class FailingJob extends Job[String, Int] {
  override def runNext(input: String) = throw new RuntimeException("something went wrong")
}

class FallbackJob extends Job[String, Int] {
  override def runNext(input: String) = throw new RuntimeException("something went wrong")

  override def onFail(exception: Throwable, input: String) = new WordCountJob().runNext(input)
}

class ConsoleJob[I](val name: String) extends SinkJob[I] {
  val messages = mutable.ListBuffer[I]()

  override def runNext(input: I) = {
    messages += input
    println(s"$name received: $input")
  }
}
