package com.canusi.os.common.job

/**
 * Copyright: Canusi
 * @author freelancer team
 * @since 2019-11-17
 * Thanks to: Dmitry Openkov
 */


import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit}
import com.canusi.os.common.util.job.AkkaStreamJob
import org.scalatest.EitherValues._
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}


class test_AkkaStreamJobNegativeSpec extends TestKit(ActorSystem("AkkaStreamJobNegativeSpec"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {

  implicit val materializer = ActorMaterializer()

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "JobBuilder" should {
    "give an error when we have cycles" in {
      val (builder, _, _, multiplyJob, squareJob, _) = test_AkkaStreamJobNegativeSpec.createBuilder()
      builder.addJobBinding(squareJob, multiplyJob)
      builder.create() must be (Left("Has cycles"))
    }
    "give an error when we connect an input to a source" in {
      val (builder, textSource, _, _, _, _) = test_AkkaStreamJobNegativeSpec.createBuilder()
      val noOp = new NoOp
      builder.addJob(noOp)
      builder.addJobBinding(noOp, textSource)
      builder.create().left.value must startWith ("Sources with predecessors")
    }
    "give an error when we connect output of a sink to a job" in {
      val (builder, _, _, _, _, consoleSink) = test_AkkaStreamJobNegativeSpec.createBuilder()
      val noOp = new NoOp
      builder.addJob(noOp)
      builder.addJobBinding(consoleSink, noOp)
      builder.create().left.value must startWith ("Sinks with successors")
    }
    "give an error when a job is not connected" in {
      val (builder, _, _, multiplyJob, _, _) = test_AkkaStreamJobNegativeSpec.createBuilder()
      val multiplyJob2 = new MultiplyJob(2)
      builder.addJob(multiplyJob2)
      builder.addJobBinding(multiplyJob, multiplyJob2)
      builder.create().left.value must startWith ("Jobs are missing bind connections")
    }
  }
}

object test_AkkaStreamJobNegativeSpec {

  //textSource -> countJob -> multiplyJob -> squareJob -> consoleSink
  def createBuilder(sleep: Int = 0) = {
    val builder = AkkaStreamJob.getJobBuilder
    val textSource = new TextSource("Simple text for processing by the jobs")
    val countJob = new WordCountJob(sleep)
    val squareJob = new SquareJob(sleep)
    val multiplyJob = new MultiplyJob(5, sleep)
    val consoleSink = new ConsoleJob[Int]("sink")
    builder.addSource(textSource)
    builder.addSink(consoleSink)
    builder.addJob(countJob)
    builder.addJob(squareJob)
    builder.addJobBinding(textSource, countJob)
    builder.addJobBinding(countJob, multiplyJob)
    builder.addJobBinding(multiplyJob, squareJob)
    builder.addJobBinding(squareJob, consoleSink)
    (builder, textSource, countJob, multiplyJob, squareJob, consoleSink)
  }
}

















